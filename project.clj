(defproject yandex-search "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
    [org.clojure/clojure "1.7.0"]
    [clj-http "2.0.0"]
    [hickory "0.5.4"]
    [org.clojure/tools.cli "0.3.2"]
    [com.cemerick/url "0.1.1"]
  ]
  :main ^:skip-aot yandex-search.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
