(ns yandex-search.core
  (:require
    [clojure.tools.cli :refer [parse-opts]]
    [clj-http.client :as client]
    [hickory.select :as s])
  (:use
    [cemerick.url :only (url)]
    [hickory.core :only (as-hickory parse)]
    [hickory.render :only (hickory-to-html)])
  (:gen-class))

(def cli-options
  [["-c" "--connections N" "Number of concurrent requests allowed."
    :default 5
    :parse-fn #(Integer/parseInt %)
    :validate [#(> % 0) "Must be a positive integer."]]
   ["-r" "--results N" "Number of results for each keyword."
    :default 10
    :parse-fn #(Integer/parseInt %)
    :validate [#(> % 0) "Must be a positive integer."]]
   ["-s" "--silent" "Do not output anything to standard output."]
   ["-h" "--help" "Display a help message."]])

(defn usage [options-summary]
  (->> ["Yandex search for multiple requests."
        "The results can be found in separate text files named results_[keyword].txt"
        "and contain meta tags together with result links."
        ""
        "Usage: yandex_search [options] keywords"
        ""
        "Options:"
        options-summary]
       (clojure.string/join \newline)))

(def results-per-page 10)

(def yandex-search-url "http://yandex.ru/search/")

(defn get-page
  "Download a single search results page.

   Options:
    :page       - which page to retrieve (starting from 0)
    :results    - how many links to get from a page
    :with-meta? - do we need to retrieve meta tags? (defaults to true when page = 0)
    :cm         - an http connection manager (optional)
    :cs         - a cookie storage (optional) "
  [kw {:keys [results cm cs page with-meta?]}]
  (let [; make page url
        page-url (-> (url yandex-search-url)
                    (assoc :query {:text kw
                                   :p page})
                    str)
        ; download and parse the results page
        page-tree (-> (client/get page-url
                                  {:connection-manager cm
                                   :cookie-store cs})
                      :body parse as-hickory)

        ; default with-meta? to true for the first page
        with-meta? (if (nil? with-meta?)
                     (= page 0)
                     with-meta?)

        ; collect meta tags if necessary
        metas (if with-meta?
                (->> (s/select (s/child
                                 (s/tag :head)
                                 (s/tag :meta))
                               page-tree))
                [])

        ; collect links
        links (->> (s/select (s/and
                               (s/class "b-link")
                               (s/tag :a))
                            page-tree)
                  (map #(:href (:attrs %)))
                  ; FIXME: there may be fewer non-ad results here (although this is very unlikely)
                  (take results)) ]
    {:meta metas
     :links links}))

(defn search-keyword
  "Retrieve Yandex search results for a keyword.

   Options:
    :results    - how many links to get for a request
    :cm         - an http connection manager (optional)
    :cs         - a cookie storage (optional) "
  [kw {:keys [results cm cs]}]
  (let [pages (quot (dec (+ results results-per-page)) results-per-page)
        last-page (mod results results-per-page)
        last-page (if (= 0 last-page) results-per-page last-page)]
    (-> (apply merge-with concat
               (for [page (range pages)]
                 (get-page kw {:results (if (= (dec pages) page)
                                          last-page
                                          results-per-page)
                               :cm cm
                               :cs cs
                               :page page})))
        (assoc :keyword kw))))

(defn spit-results
  "Spit search results in a separate file."
  [results]
  (let [{kw     :keyword
         metas  :meta
         links  :links} results]
    (spit (format "results_%s.txt" kw)
          (clojure.string/join
            \newline
            (concat
              (map hickory-to-html metas)
              links)))))

(defn search-keywords
  "Retrieve Yandex search results for multiple keywords in parallel.

   Options:
    :results      - how many links to get for a request
    :connections  - how many simultaneous http requests are allowed"
  [kws {:keys [results connections]}]
  (let [; create an agent for each search request
        agents (map agent kws)
        ; an http connection manager
        cm (clj-http.conn-mgr/make-reusable-conn-manager {:timeout 2 :threads connections})
        ; a common cookie store
        cs (clj-http.cookies/cookie-store)]

    ; fetch search results in parallel
    (doseq [agent agents]
      (send-off agent search-keyword {:results results
                                      :cm cm
                                      :cs cs})
      (send-off agent spit-results))

    ; wait for the agents to finish
    (doall (map await agents))

    ; shutdown http connection manager after we're done
    (clj-http.conn-mgr/shutdown-manager cm)))

(defn exit
  "Exit a program with an exit message and exit status."
  [status msg]
  (println msg)
  (System/exit status))

(defn -main
  "Yandex search for multiple requests."
  [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (< (count arguments) 1) (exit 1 (usage summary)))

    ; display execution options
    (when (not (:silent options))
      (println "Results per keyword:" (:results options))
      (println "Maximum connections:" (:connections options))
      (println "Searching for:")
      (doseq [kw arguments]
        (println kw)))

    ; perform search
    (search-keywords arguments
                     {:results (:results options)
                      :threads (:connections options)})

    (shutdown-agents)))

